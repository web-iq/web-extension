import React from 'react'
import { GoLinkExternal } from 'react-icons/go'

type props = {
  url: string
  title: string
  type: string
}

const RedirectButton = (props: props) => {
  return (
    <>
      {props.type === 'link' ? (
        <a target="_blank" href={props.url}>
          <div className="group flex justify-between items-center relative border-[1px] hover:cursor-pointer p-2 hover:bg-gray-100 z-100">
            <p className="">{props.title}</p>

            <GoLinkExternal className="group-hover:block hidden" />
          </div>
        </a>
      ) : (
        <div
          onClick={() => {
            chrome.runtime.openOptionsPage()
          }}
          className="group flex justify-between items-center relative border-[1px] hover:cursor-pointer p-2 hover:bg-gray-100 z-100"
        >
          <p className="">{props.title}</p>

          <GoLinkExternal className="group-hover:block hidden" />
        </div>
      )}
    </>
  )
}

export default RedirectButton
