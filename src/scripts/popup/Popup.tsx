import React, { useEffect, useState } from 'react'
import WebIQLogo from '@/assets/webiq-logo.png'
import ToggleButton from '@/components/ToggleButton.js'
import ChartComponent from '@/components/ChartComponent'
import RedirectButton from '@/components/RedirectButton'
import {
  EXTENSION_STATE_KEY,
  EXTENSION_STATE_OFF,
  EXTENSION_STATE_ON,
  EXTENSION_STATE_UPDATE_COMMAND,
  TOKEN_STATE_KEY
} from '@/utils/constants'
import { getItem, setItem } from '@/utils/storage'
import { IoSettingsOutline } from 'react-icons/io5'
import { MdOutlineDashboard } from 'react-icons/md'
import Dropdown from '@/components/DropDown'
import { formatTime } from '@/utils/formatTime'

const Popup = () => {
  const [activeTab, setActiveTab] = useState<chrome.tabs.Tab>(null)
  const [authState, setAuthState] = useState<boolean>(false)
  const [extensionState, setExtensionState] = useState<string>(EXTENSION_STATE_OFF)

  async function getCurrentTab() {
    let queryOptions = { active: true, lastFocusedWindow: true }

    let [tab] = await chrome.tabs.query(queryOptions)
    setActiveTab(tab)
  }

  useEffect(() => {
    getItem(TOKEN_STATE_KEY).then(state => {
      if (state === undefined) {
        setAuthState(false)
      } else {
        setAuthState(true)
      }
    })

    getItem(EXTENSION_STATE_KEY).then(state => {
      if (state === undefined) {
        setExtensionState(state)
      } else {
        setExtensionState(state)
      }
    })

    getCurrentTab()
  }, [])

  useEffect(() => {    
    chrome.runtime.sendMessage({
      command: EXTENSION_STATE_UPDATE_COMMAND,
      state: extensionState
    })
  }, [extensionState])

  return (
    <div className="w-[400px] max-h-[600px] h-[100%] overflow-y-scroll p-3 pt-0 bg-white text-black">
      <Header
        authState={authState}
        extensionState={extensionState}
        setExtensionState={setExtensionState}
      />

      {!authState && (
        <div className="w-[100%] h-[300px] flex justify-center items-center">
          <button
            onClick={() => {
              chrome.runtime.openOptionsPage()
            }}
            className="bg-skyBlueShade px-3 py-2 rounded-full"
          >
            Login to Start
          </button>
        </div>
      )}

      {authState && extensionState === EXTENSION_STATE_OFF && (
        <div className="w-[100%] h-[300px] flex justify-center items-center">
          <p>Please Turn on the Extension.</p>
        </div>
      )}

      {authState && extensionState === EXTENSION_STATE_ON && (
        <>
          <BottomHeader />
          <ChartSection />
          <InfoSection />
        </>
      )}
    </div>
  )
}

export default Popup

export const Header = ({ authState, extensionState, setExtensionState }) => {
  return (
    <div className="flex sticky top-0 z-50 pt-3 bg-white justify-between items-end px-3 border-b-[1px] pb-3">
      <div className="flex items-end">
        <img className="h-[40px]" src={WebIQLogo} />
        <p className="font-semibold text-blueShade ml-1 mb-1">for chrome</p>
      </div>

      {authState && (
        <div>
          <ToggleButton extensionState={extensionState} setExtensionState={setExtensionState} />
        </div>
      )}
    </div>
  )
}

export const BottomHeader = () => {
  return (
    <div className="p-3 flex items-center justify-between">
      <p className="font-semibold text-xl">Your Activity</p>

      <div className="flex text-xl items-center">
        <div className="flex bg-skyBlueShade text-black px-2 py-1 mr-3 rounded-full hover:bg-opacity-50 cursor-pointer">
          <MdOutlineDashboard />
          <p className="text-sm ml-1">Dashboard</p>
        </div>

        <IoSettingsOutline
          onClick={() => {
            chrome.runtime.openOptionsPage()
          }}
          className="cursor-pointer hover:scale-105 hover:rotate-12"
        />
      </div>
    </div>
  )
}

export const ChartSection = () => {
  return (
    <div className="p-3">
      <div className="flex justify-center w-[100%] items-center h-[300px]">
        <ChartComponent />
      </div>
    </div>
  )
}

export const ButtonSection = () => {
  return (
    <div className="mt-[-60px]">
      <div className="mb-2">
        <RedirectButton type="link" title="Go to Dashboard" url="https://www.google.com" />
      </div>

      <div className="mb-2">
        <RedirectButton title="Go to Settings Page" type="button" url="" />
      </div>
    </div>
  )
}

export const InfoSection = () => {
  const logs = [
    { id: 1, domain: 'google.com', time: 1200 },
    { id: 2, domain: 'netflix.com', time: 4200 },
    { id: 3, domain: 'facebook.com', time: 2200 },
    { id: 4, domain: 'instagram.com', time: 800 }
  ]

  return (
    <div className="mt-[-40px]">
      <div className="flex justify-between items-center pr-3 border-b-[1px]">
        <Dropdown />

        <div>
          <p>{formatTime(342070)}</p>
        </div>
      </div>

      <div className="px-3 mt-1">
        {logs.map((log, i) => (
          <div
            className={`flex px-2 justify-between ${
              i % 2 ? 'bg-skyBlueShade bg-opacity-40' : 'bg-white'
            }`}
          >
            <p className="text-[12px]">{log.domain}</p>

            <p className="text-[12px]">{formatTime(log.time)}</p>
          </div>
        ))}
      </div>
    </div>
  )
}
