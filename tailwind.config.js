/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        whiteShade: "#fafafa",
        offWhiteShade: "#f1f1f1",
        blackShade: "#060304",
        skyBlueShade: "#98dbe4",
        blueShade: "#99bde4",
        skinShade: "#fec0a7",
        greenShade: "#b0d1ac",
        redShade: "#ff403b"
      }
    }
  },
  plugins: []
}
