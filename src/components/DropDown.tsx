import React, { useState } from 'react';

const Dropdown = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState({ id: 1, label: 'Today\'s' });

  const options = [
    { id: 1, label: 'Today\'s' },
    { id: 2, label: 'Yesterday\'s' },
    { id: 3, label: 'This Week\'s' },
    { id: 4, label: 'Last Week\'s' },
    { id: 5, label: 'This Month\'s' },
    { id: 6, label: 'Last Month\'s' },
  ];

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option);
    setIsOpen(false);
  };

  return (
    <div className="relative inline-block text-left">
      <div>
        <button
          type="button"
          className="flex scale-90 w-[180px] justify-between px-5 py-1 text-sm font-medium text-gray-700 bg-skyBlueShade border border-gray-300 rounded-full hover:bg-opacity-75 focus:outline-none focus:ring-2  "
          onClick={toggleDropdown}
        >
          {selectedOption ? selectedOption.label : 'Select an option'}
          <svg
            className="-mr-1 scale-75 ml-2 h-5 w-5"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
            fill="currentColor"
            aria-hidden="true"
          >
            <path
              fillRule="evenodd"
              d="M10 12l-6-6 1.5-1.5L10 9l4.5-4.5L16 6l-6 6z"
              clipRule="evenodd"
            />
          </svg>
        </button>
      </div>

      {isOpen && (
        <div className="absolute scale-90 left-[-10px] mt-2 w-[180px] origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5">
          <div className="py-1 h-[70px] overflow-y-scroll">
            {options.map((option) => (
              <button
                key={option.id}
                className="block w-full px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                onClick={() => handleOptionClick(option)}
              >
                {option.label}
              </button>
            ))}
          </div>
        </div>
      )}
    </div>
  );
};

export default Dropdown;
