import React from 'react'
import {
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
  ResponsiveContainer
} from 'recharts'

const data = [
  {
    label: 'Productive',
    A: 100
  },
  {
    label: 'Social Media',
    A: 50
  },
  {
    label: 'Entertainment',
    A: 86,
    B: 100,
    fullMark: 100
  },
  {
    label: 'News & Info',
    A: 99,
    B: 100,
    fullMark: 100
  },
  {
    label: 'E-Commerce',
    A: 85,
    B: 90,
    fullMark: 100
  }
]

const ChartComponent = () => {
  return (
    <div className="w-[100%] h-[100%] mt-[-30px] z-1">
      <ResponsiveContainer width="100%" height="100%">
        <RadarChart cx="50%" cy="50%" outerRadius="50%" data={data}>
          <PolarGrid />
          <PolarAngleAxis dataKey="label" />
          <PolarRadiusAxis />
          <Radar name="Mike" dataKey="A" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
        </RadarChart>
      </ResponsiveContainer>
    </div>
  )
}

export default ChartComponent
