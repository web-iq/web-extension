import React, { useEffect, useState } from 'react'
import LoginPage from './components/LoginPage'
import SettingsPage from './components/SettingsPage'
import { getItem, removeItem } from '@/utils/storage'
import { TOKEN_STATE_KEY } from '@/utils/constants'

const Options = () => {
  const [authState, setAuthState] = useState(false)

  useEffect(() => {
    getItem(TOKEN_STATE_KEY).then((state) => {
      if(state === undefined) {
        setAuthState(false)
      } else {
        setAuthState(true)
      }
    })
  }, [])

  return (
    <div className="w-[100vw] h-[100vh]">
      {authState === false && <LoginPage setAuthState={setAuthState} />}
      {authState === true && <SettingsPage setAuthState={setAuthState} />}
    </div>
  )
}

export default Options