import { EXTENSION_STATE_KEY, EXTENSION_STATE_OFF, EXTENSION_STATE_ON, EXTENSION_STATE_UPDATE_COMMAND, TOKEN_STATE_KEY } from "@/utils/constants"
import { getItem, removeItem, setItem } from "@/utils/storage"
import { initializeListeners } from "@/utils/timeTracker"

console.log('Background Service Worker Loaded')

chrome.runtime.onInstalled.addListener(async () => {
  removeItem(TOKEN_STATE_KEY)
  chrome.runtime.openOptionsPage()
  chrome.action.setBadgeText({ text: "" });
  setItem(EXTENSION_STATE_KEY, EXTENSION_STATE_OFF);
})

function setExtensionState(state) {
  chrome.action.setBadgeText({ text: state });
  setItem(EXTENSION_STATE_KEY, state);
}

chrome.action.onClicked.addListener(() => {
  chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
    const activeTab = tabs[0]
    chrome.tabs.sendMessage(activeTab.id!, { message: 'clicked_browser_action' })
  })
})

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  const { command } = message
  switch (command) {
    case 'hello-world':
      console.log('Hello World, from the Background Service Worker')
      sendResponse({ success: true, message: 'Hello World' })
      break
    case EXTENSION_STATE_UPDATE_COMMAND: 
      setExtensionState(message.state);

      getItem(TOKEN_STATE_KEY).then((data) => {
        if(data !== undefined) {
          if(message.state === EXTENSION_STATE_ON) {
            initializeListeners()
          } 
        }
      })
      
      break
    default:
      break
  }
})

chrome.commands.onCommand.addListener(command => {
  console.log(`Command: ${command}`)

  if (command === 'refresh_extension') {
    chrome.runtime.reload()
  }
})

export {}
