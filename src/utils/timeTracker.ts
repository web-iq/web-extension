import axios from 'axios'
import { setItem, getItem, removeItem } from './storage'
import { STATE_DOMAIN, STATE_DURATION, STATE_TIME, TOKEN_STATE_KEY } from './constants'
import { debounce } from 'lodash'

// Function to extract domain from URL
const getDomainFromUrl = url => {
  const domainRegex = /^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n?]+)/g
  const matches = domainRegex.exec(url)
  return matches && matches.length > 1 ? matches[1] : null
}

// Function to create a visit log via API request
const createVisitLog = async (domain, duration, visited_at) => {
  getItem(TOKEN_STATE_KEY).then(data => {
    console.log(data)
    fetch('http://localhost:3434/visitlog/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        token: data // Assuming data is your token variable
      },
      body: JSON.stringify({
        domain: domain,
        duration: duration,
        visited_at: visited_at
      })
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to create visit log')
        }
        // Handle success if needed
      })
      .catch(error => {
        console.error('Error creating visit log:', error)
      })
  })
}

// Function to initialize tracking
const initializeTracker = () => {
  // Load variables from storage
  getItem(STATE_DOMAIN).then(domain => {
    getItem(STATE_TIME).then(visited_at => {
      if (domain && visited_at) {
        console.log('Resuming tracking')
      } else {
        console.log('Initializing tracking')
      }
    })
  })
}

// Debounced function to create visit log
const debouncedCreateVisitLog = debounce(createVisitLog, 1000)

// Function to start tracking
const startTracking = () => {
  chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
    if (tabs && tabs.length > 0) {
      const activeTab = tabs[0]
      const url = activeTab.url
      const domain = getDomainFromUrl(url)
      getItem(STATE_TIME).then(visited_at => {
        if (visited_at) {
          const duration = Date.now() - visited_at;
          setItem(STATE_DOMAIN, domain)
          setItem(STATE_TIME, Date.now())
          console.log('Tracking started for domain:', domain)
          debouncedCreateVisitLog(domain, duration, visited_at)
        } else {
          console.log('No previous visit recorded. Tracking started for domain:', domain)
          setItem(STATE_DOMAIN, domain)
          setItem(STATE_TIME, Date.now())
        }
      })
    }
  })
}

// Function to stop tracking
const stopTracking = () => {
  getItem(STATE_DOMAIN).then(domain => {
    getItem(STATE_TIME).then(visited_at => {
      const endTime = Date.now()
      const duration = endTime - visited_at
      createVisitLog(domain, duration, visited_at) // Create visit log
      removeItem(STATE_DOMAIN)
      removeItem(STATE_TIME)
      console.log('Tracking stopped for domain:', domain)
    })
  })
}

// Main function to track time
const trackTime = action => {
  switch (action) {
    case 'start':
      startTracking()
      break
    case 'stop':
      stopTracking()
      break
    case 'initialize':
      initializeTracker()
      break
    default:
      console.error('Invalid action:', action)
  }
}

// Function to handle tab change
const handleTabChange = () => {
  chrome.tabs.onActivated.addListener(tab => {
    trackTime('stop') // Stop tracking for previous tab
    trackTime('start') // Start tracking for new tab
  })
}

// Function to handle out-of-focus tab
const handleTabBlur = () => {
  chrome.tabs.onActivated.addListener(tab => {
    trackTime('stop') // Stop tracking for current tab
  })
}

// Function to handle URL change
const handleUrlChange = () => {
  chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (changeInfo.url) {
      trackTime('stop') // Stop tracking for current tab
      trackTime('start') // Start tracking for new URL
    }
  })
}

// Function to handle window close
const handleWindowClose = () => {
  chrome.windows.onRemoved.addListener(windowId => {
    trackTime('stop') // Stop tracking when window is closed
  })
}

// Function to handle active tab change
const handleActiveTabChange = () => {
  chrome.windows.onFocusChanged.addListener(windowId => {
    if (windowId === chrome.windows.WINDOW_ID_NONE) {
      // No focused window
      trackTime('stop') // Stop tracking when no window is focused
    } else {
      // A window is focused
      chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
        if (tabs && tabs.length > 0) {
          trackTime('start') // Start tracking for active tab
        }
      })
    }
  })
}


// Initialize event listeners
export const initializeListeners = () => {
  handleTabChange()
  handleActiveTabChange()
  handleTabBlur()
  handleUrlChange()
}

