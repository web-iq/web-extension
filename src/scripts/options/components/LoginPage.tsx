import { TOKEN_STATE_KEY } from '@/utils/constants'
import { setItem } from '@/utils/storage'
import axios from 'axios'
import React, { useState } from 'react'

const LoginPage = ({setAuthState}) => {
  const [isLoginPage, setIsLoginPage] = useState(true)

  return (
    <div className="relative overflow-hidden w-[100%] h-[100%] flex justify-center items-center">
      <img
        className="absolute"
        src="https://images.pexels.com/photos/434645/pexels-photo-434645.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
      />

      {isLoginPage === true && <Login setIsLoginPage={setIsLoginPage} setAuthState={setAuthState} />}

      {isLoginPage === false && <Register setIsLoginPage={setIsLoginPage} />}
    </div>
  )
}

export default LoginPage

const Login = ({ setIsLoginPage, setAuthState }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleSubmit = async () => {
    const response = await axios.post('http://localhost:3434/user/login', {
      email: email,
      password: password
    })

    setItem(TOKEN_STATE_KEY, response.data.token).catch(error => {
      console.error('Error setting token:`', error)
    })

    setAuthState(true)
  }

  return (
    <div className="absolute w-[450px] h-[300px] bg-white p-5 rounded-md">
      <p className="text-xl mb-3">Login</p>
      <input
        placeholder="email"
        className="border-[1px] w-[100%] px-3 py-2 outline-none rounded-md mb-3"
        onChange={e => setEmail(e.target.value)}
      />
      <input
        placeholder="password"
        className="border-[1px] w-[100%] px-3 py-2 outline-none rounded-md mb-3"
        type="password"
        onChange={e => setPassword(e.target.value)}
      />

      <button className="bg-skyBlueShade px-2 py-1 rounded-full" onClick={handleSubmit}>
        Submit
      </button>

      <div className="flex flex-col items-start mt-5">
        <p className="text-[12px] mt-3">
          Don't have an account?{' '}
          <span
            onClick={() => {
              setIsLoginPage(false)
            }}
            className="text-skyBlueShade cursor-pointer"
          >
            Register
          </span>
        </p>

        <p className="text-[12px]">
          Forgot Password?{' '}
          <span
            onClick={() => {
              setIsLoginPage(false)
            }}
            className="text-skyBlueShade cursor-pointer"
          >
            Click Here
          </span>
        </p>
      </div>
    </div>
  )
}

const Register = ({ setIsLoginPage }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [username, setUsername] = useState('')

  const handleSubmit = async () => {
    const response = await axios.post('http://localhost:3434/user/register', {
      email: email,
      password: password,
      username: username
    })

    console.log(response)

    setItem(TOKEN_STATE_KEY, response.data.token).catch(error => {
      console.error('Error setting token:`', error)
    })
  }

  return (
    <div className="absolute w-[450px] h-[300px] bg-white p-5 rounded-md">
      <p className="text-xl mb-3">Register</p>

      <input
        placeholder="username"
        className="border-[1px] w-[100%] px-3 py-2 outline-none rounded-md mb-3"
        onChange={e => setUsername(e.target.value)}
      />

      <input
        placeholder="email"
        className="border-[1px] w-[100%] px-3 py-2 outline-none rounded-md mb-3"
        onChange={e => setEmail(e.target.value)}
      />
      <input
        placeholder="password"
        className="border-[1px] w-[100%] px-3 py-2 outline-none rounded-md mb-3"
        type="password"
        onChange={e => setPassword(e.target.value)}
      />

      <button className="bg-skyBlueShade px-2 py-1 rounded-full" onClick={handleSubmit}>
        Submit
      </button>

      <div>
        <p className="text-[12px] mt-3">
          Already have an account?{' '}
          <span
            onClick={() => {
              setIsLoginPage(true)
            }}
            className="text-skyBlueShade cursor-pointer"
          >
            Login
          </span>
        </p>
      </div>
    </div>
  )
}
