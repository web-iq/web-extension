export const formatTime = (seconds) => {
  // Calculate hours and minutes
  const hours = Math.floor(seconds / 3600);
  const remainingSeconds = seconds % 3600;
  const minutes = Math.round(remainingSeconds / 60);

  // Build the string representation
  let result = '';
  if (hours > 0) {
    result += `${hours}h`;
    if (minutes > 0) {
      result += ` ${minutes}m`;
    }
  } else {
    result += `${minutes}m`;
  }

  return result;
}
